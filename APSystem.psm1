Set-StrictMode -Version 5.0
$ErrorActionPreference = "Stop"

$script:ThisDir = Split-Path $MyInvocation.MyCommand.Path -Parent
$script:SettingsFile = Join-Path $ThisDir "settings.json"

function Install-APSystem {
    [CmdletBinding()]
    param()

    Set-StrictMode -Version 5.0
    $ErrorActionPreference = "Stop"

    $deviceId = Read-Host "Enter Particle device ID"
    $particleAccessToken = Read-Host "Enter Particle device access token" -AsSecureString
    $thingSpeakChannel = Read-Host "Enter ThingSpeak channel ID"
    $thingSpeakKey = Read-Host "Enter ThingSpeak channel Read API Key" -AsSecureString

    @{
        DeviceId = $deviceId;
        ParticleAccessToken = $particleAccessToken | ConvertFrom-SecureString;
        ThingSpeakChannel = $thingSpeakChannel;
        ThingSpeakKey = $thingSpeakKey | ConvertFrom-SecureString;
    } | ConvertTo-Json | Out-File $SettingsFile
}

if (!(Test-Path $SettingsFile)) {
    Install-APSystem
}

$script:Settings = Get-Content $SettingsFile -Raw | ConvertFrom-Json
$script:ParticleAccessToken = $Settings.ParticleAccessToken | ConvertTo-SecureString
$script:ThingSpeakKey = $Settings.ThingSpeakKey | ConvertTo-SecureString
$script:ParticleApiUrl = "https://api.particle.io/v1/devices/$($Settings.DeviceId)/"
$script:ThingSpeakApiUrl = "https://api.thingspeak.com/channels/$($Settings.ThingSpeakChannel)/"

function getParticleAccessToken {
    [CmdletBinding()]
    param()

    $BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($ParticleAccessToken)
    [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)
}

function getThingSpeakReadKey {
    [CmdletBinding()]
    param()

    $BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($ThingSpeakKey)
    [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)
}

function callParticleFunction {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$FunctionName,

        [Parameter()]
        [string]$Arguments
    )

    $funcUrl = $ParticleApiUrl + $FunctionName
    $requestBody = @{ "access_token" = getParticleAccessToken; }

    if ($Arguments) {
        $requestBody["args"] = $Arguments
    }

    $apiResponse = (Invoke-WebRequest -Uri $funcUrl -Method Post -Body $requestBody).Content `
        | ConvertFrom-Json

    $apiResponse
}

function getParticleVariable {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$VariableName
    )

    $variableUrl = $ParticleApiUrl + $VariableName
    $requestBody = @{ "access_token" = getParticleAccessToken; }
    $apiResponse = (Invoke-WebRequest -Uri $variableUrl -Method Get -Body $requestBody).Content `
        | ConvertFrom-Json

    $apiResponse
}

function getThingSpeakFeed {
    [CmdletBinding()]
    param()

    $statusUrl = $ThingSpeakApiUrl + "feed.json"
    $requestBody = @{ "api_key" = getThingSpeakReadKey }
    $apiResponse = (Invoke-WebRequest -Uri $statusUrl -Method Get -Body $requestBody).Content `
        | ConvertFrom-Json

    $apiResponse
}

function Get-SumpWaterLevel {
    [CmdletBinding()]
    param()

    Set-StrictMode -Version 5.0
    $ErrorActionPreference = "Stop"

    (getParticleVariable "sump-level").result
}

function Get-LightState {
    [CmdletBinding()]
    param()

    Set-StrictMode -Version 5.0
    $ErrorActionPreference = "Stop"

    (getParticleVariable "lights").result
}

function Get-WiFiRssi {
    [CmdletBinding()]
    param()

    $ErrorActionPreference = "Stop"
    Set-StrictMode -Version 5.0

    (getParticleVariable "rssi").result
}

function Set-LightState {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)]
        [ValidateSet("On", "Off")]
        [string]$State
    )

    Set-StrictMode -Version 5.0
    $ErrorActionPreference = "Stop"

    $result = callParticleFunction "lights" $State
    if ($result.return_value -ne 0) {
        throw "Could not set grow light state: Photon return value $($result.return_value)"
    }
}

function Get-SumpWaterLevelLog {
    [CmdletBinding()]
    param()

    Set-StrictMode -Version 5.0
    $ErrorActionPreference = "Stop"

    $log = @((getThingSpeakFeed).feeds | foreach {

        $waterLevel = [System.Net.WebUtility]::HtmlDecode($_.field1)
        $time = [DateTime]::Parse($_.created_at)

        if ($waterLevel) {

            $eventObj = $waterLevel | ConvertFrom-Json
            $waterLevel = $eventObj.data

            $timeStr = $eventObj.time
            if ($timeStr -and $timeStr.ToLower() -ne "unknown") {
                $time = [DateTime]::Parse($timeStr) # This time is more precise than created_at
            }
        }

        [pscustomobject]@{
            Time = $time;
            Id = $_.entry_id;
            WaterLevel = $waterLevel;
        }
    } `
    | where { $_.WaterLevel } `
    | sort Time)

    for ($i = 0; $i -lt $log.Count - 1; $i++)
    {
        $current = $log[$i]
        $next = $log[$i + 1]
        $duration = $next.Time - $current.Time

        $current | Add-Member -NotePropertyName "Duration" -NotePropertyValue $duration
    }

    if ($log.Count -gt 0) {

        # We don't want to ignore the last item in the log
        $now = Get-Date
        $lastLogItem = $log[-1]
        $duration = $now - $lastLogItem.Time
        $lastLogItem | Add-Member -NotePropertyName "Duration" -NotePropertyValue $duration
    }

    $log
}

function Get-APStatus {
    [CmdletBinding()]
    param()

    $requestBody = @{ "access_token" = getParticleAccessToken; }
    $apiResponse = (Invoke-WebRequest -Uri $ParticleApiUrl -Method Get -Body $requestBody).Content `
        | ConvertFrom-Json

    #id                   : 3b001d001047343432313031
    #name                 : AP01
    #last_app             :
    #last_ip_address      : 72.192.89.90
    #last_heard           : 2016-05-15T23:11:18.856Z
    #product_id           : 6
    #connected            : False
    #platform_id          : 6
    #cellular             : False
    #status               : normal
    #variables            :
    #functions            :
    #cc3000_patch_version : wl0: Nov  7 2014 16:03:45 version 5.90.230.12 FWID 01-cb2cb710

    [pscustomobject]@{
        LastHeard = [DateTime]::Parse($apiResponse.last_heard);
        IsConnected = [bool]$apiResponse.connected;
    }
}

function Get-SumpWaterLevelStats {
    [CmdletBinding()]
    param(
        [Parameter()]
        [psobject[]]$Log,

        [Parameter()]
        [double]$HoursToConsider = 8.0
    )

    if ($HoursToConsider -le 0.0) {
        throw "HoursToConsider must be greater than 0."
    }

    if (!$Log) {
        $Log = Get-SumpWaterLevelLog
    }

    $now = Get-Date
    $logStartTime = $now - [TimeSpan]::FromHours($HoursToConsider)

    $trimmedLog = $Log | where { $_.Time + $_.Duration -ge $logStartTime } `
        | where { $_.Time -le $now }

    $allWaterLevels = @{
        "above-threshold" = 0.0;
        "below-threshold" = 0.0;
    }

    $trimmedLog | where { $_.WaterLevel } `
        | where { $allWaterLevels.ContainsKey($_.WaterLevel) } `
        | foreach { $allWaterLevels[$_.WaterLevel] += $_.Duration.TotalSeconds }

    $aboveThresholdTime = $allWaterLevels["above-threshold"]
    $belowThresholdTime = $allWaterLevels["below-threshold"]
    $totalWaterLevelTime = ($allWaterLevels.Values | Measure-Object -Sum).Sum

    $results = [pscustomobject]@{
        "AboveThreshold (%)" = "NA";
        "BelowThreshold (%)" = "NA";
    }

    if ($totalWaterLevelTime -eq 0) {
        Write-Warning "No water levels were logged within the last $HoursToConsider hours."
    }
    else {
        $results."AboveThreshold (%)" = [Math]::Round(($aboveThresholdTime / $totalWaterLevelTime) * 100.0, 1)
        $results."BelowThreshold (%)" = [Math]::Round(($belowThresholdTime / $totalWaterLevelTime) * 100.0, 1)
    }

    $results
}

function Restart-APSystem {
    [CmdletBinding()]
    param()

    callParticleFunction "reset"
}

function isConnected() {
    try {
        (Get-APStatus).IsConnected
    }
    catch {
        $false
    }
}

function Wait-APSystemConnected {
    [CmdletBinding()]
    param(
        [Parameter()]
        [scriptblock]$Action
    )

    while (-not (isConnected)) {
        Start-Sleep -Seconds 30
    }

    Write-Verbose "AP system came online at $(Get-Date)"

    if ($Action) {
        & $Action
    }
}

"Install-APSystem",
    "Get-SumpWaterLevel",
    "Get-LightState",
    "Set-LightState",
    "Get-WiFiRssi",
    "Get-SumpWaterLevelLog",
    "Get-APStatus",
    "Get-SumpWaterLevelStats",
    "Restart-APSystem",
    "Wait-APSystemConnected" `
    | foreach { Export-ModuleMember $_ }
